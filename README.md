# UBB #

***

+ Semester 3
	* [Advanced Methods of Programming](https://bitbucket.org/Mateioo7/apm/src/master/) (aka FP3)
	* [Computer Networks](https://bitbucket.org/Mateioo7/cn/src/master/)
	* [Databases](https://bitbucket.org/Mateioo7/db/src/master/)
	* [Functional and Logic Programming](https://bitbucket.org/Mateioo7/flp/src/master/)
	* [Probabilities and Statistics](https://bitbucket.org/Mateioo7/ps/src/master/)
	
+ Semester 4
	* [Artificial Intelligence](https://bitbucket.org/Mateioo7/ai/src/master/)
	* [Web Programming](https://bitbucket.org/Mateioo7/wp/src/master/)
	
+ Semester 5 (2021-2022)
	* [Cloud Application Architecture](https://bitbucket.org/Mateioo7/cloud/src/master/) (Horea-Adrian Grebla)
	* [Formal Languages and Compiler Design](https://bitbucket.org/Mateioo7/flcd/src/master/) (Simona Motogna)
	* [Mobile Applications](https://bitbucket.org/Mateioo7/ma/src/master/) (Ioan Lazăr)
	* [Parallel and Distributed Programming](https://bitbucket.org/Mateioo7/pdp/src/master/) (Radu-Lucian Lupșa)
	* [Robotic Process Automation](https://bitbucket.org/Mateioo7/rpa/src/master/) (Camelia Chisăliţă-Creţu)